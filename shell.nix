{ pkgs ? import <nixpkgs> {  } }:

with pkgs;
mkShell {
  name = "kite-dev";
  buildInputs = with pkgs; [
    ghc
    stack
    haskell-language-server
    hlint
    haskellPackages.brittany
  ];
}
