{-# LANGUAGE LambdaCase #-}

module Util
    ( parseJSONFile
    ) where

import           Data.Yaml                      ( decodeFileEither )
import           Data.Yaml.TH                   ( FromJSON )

parseYAMLFile :: (FromJSON j) => FilePath -> IO j
parseYAMLFile file = decodeFileEither file >>= \case
    Left  _   -> error "Unable to parse json"
    Right res -> return res
