{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}

module Kite.Theme
    ( getTheme
    , Theme(..)
    , Colors(..)
    , ColorVar(..)
    ) where

import           Control.Monad                  ( filterM
                                                , mfilter
                                                )
import           Data.Aeson
import           Data.Yaml
import           GHC.Generics                   ( Generic )
import           Kite.Config
import           System.Directory               ( doesFileExist )
import           Util                           ( parseYAMLFile )

data ColorVar = ColorVar
    { black   :: String
    , red     :: String
    , green   :: String
    , yellow  :: String
    , blue    :: String
    , magenta :: String
    , cyan    :: String
    , white   :: String
    }
    deriving (Show, Generic)

data Colors = Colors
    { background :: String
    , foreground :: String
    , normal     :: ColorVar
    , bright     :: ColorVar
    }
    deriving (Show, Generic)

instance FromJSON ColorVar
instance FromJSON Colors

data Theme = Theme
    { wallpaper :: Maybe FilePath
    , colors    :: Colors
    }
    deriving Show

getThemePath :: Config -> String -> FilePath
getThemePath _    []     = error "No theme provided"
getThemePath conf search = themeLocation conf ++ "/" ++ search

getWallpaper :: FilePath -> IO (Maybe FilePath)
getWallpaper p = walls >>= \case
    []       -> return Nothing
    (x : xs) -> return $ Just x
  where
    walls = filterM doesFileExist $ map (p <> "wallpaper." ++) ext
    ext   = ["png", "jpg", "jpeg", "webm"]

getTheme :: Config -> String -> IO Theme
getTheme conf search = do
    colors <- parseYAMLFile $ theme ++ "/colors.yml"
    wall   <- getWallpaper theme

    return Theme { wallpaper = wall, colors = colors }
    where theme = getThemePath conf search
