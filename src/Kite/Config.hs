{-# LANGUAGE DeriveGeneric #-}

module Kite.Config
  ( initConfig
  , Config(..)
  ) where

import           Control.Monad                  ( unless )
import           Data.Aeson
import           Data.Yaml
import           GHC.Generics
import           System.Directory
import           Util                           ( parseYAMLFile )

data Config = Config
  { defaultTheme  :: String
  , themeLocation :: FilePath
  }
  deriving (Show, Generic)

instance FromJSON Config where
  parseJSON =
    genericParseJSON defaultOptions { fieldLabelModifier = camelTo2 '_' }

initConfig :: IO Config
initConfig = do
  -- Create ~/.config/kite if it is missing
  createDirectoryIfMissing True <$> configDir

  -- Create ~/.config/kite/kite.yml if it is missing
  conf <- configFile
  doesFileExist conf >>= \x -> unless x $ writeFile
    conf
    "default_theme: \"\"\ntheme_location: \"/home/rain/themes\""

  -- Return the parsed config
  parseYAMLFile conf
 where
  configDir  = (++ "/.config/kite") <$> getHomeDirectory
  configFile = (++ "/kite.yml") <$> configDir
